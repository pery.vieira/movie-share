package com.b1thouse.movieshare

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MovieShareApplication

fun main(args: Array<String>) {
	runApplication<MovieShareApplication>(*args)
}
