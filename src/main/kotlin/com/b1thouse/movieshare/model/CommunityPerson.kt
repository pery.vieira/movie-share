package com.b1thouse.movieshare.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import java.time.LocalDateTime

class CommunityPerson(
        @Id
        val id: Long,
        @Column("person_id")
        val personId: Long,
        @Column("community_id")
        val communityId: Long
)