package com.b1thouse.movieshare.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime


@Table("person")
data class Person (
        @Id
        val id: Long,
        @Column("name")
        val name: String,
        @Column("code")
        var code: String?,
        @Column("created_at")
        val createdAt: LocalDateTime? = LocalDateTime.now(),
        @Column("updated_at")
        val updatedAt: LocalDateTime? = LocalDateTime.now(),
        @Column("actived")
        val actived: Boolean? = true
)