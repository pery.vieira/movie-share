package com.b1thouse.movieshare.exception

data class PersonNotFoundException(
        override val type: TypeError = TypeError.NOT_FOUND,
        override val message: String = type.message
): BaseException()