package com.b1thouse.movieshare.exception

class CommunityNotFoundException(
        override val type: TypeError = TypeError.NOT_FOUND,
        override val message: String = type.message
): BaseException()