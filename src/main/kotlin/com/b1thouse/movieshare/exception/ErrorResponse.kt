package com.b1thouse.movieshare.exception

import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.validation.Valid

data class ErrorResponse(
        @JsonProperty("type")
        val type: String? = null,
        @JsonProperty("message")
        val message: String? = null,
        @Valid
        @JsonProperty("details")
        val details: Any? = null
) {
}