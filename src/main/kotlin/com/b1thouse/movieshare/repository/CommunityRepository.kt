package com.b1thouse.movieshare.repository

import com.b1thouse.movieshare.model.Community
import org.springframework.stereotype.Repository

@Repository
interface CommunityRepository {
    fun create(community: Community)
    fun findByCode(code: String): String?
    fun findById(id: Long): Community?
}