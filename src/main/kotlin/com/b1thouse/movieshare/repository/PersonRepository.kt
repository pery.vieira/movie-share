package com.b1thouse.movieshare.repository

import com.b1thouse.movieshare.model.Person
import org.springframework.stereotype.Repository

@Repository
interface PersonRepository {
    fun create(person: Person)
    fun findByCode(code: String): String?
    fun findById(id: Long): Person?
}