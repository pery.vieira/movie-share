package com.b1thouse.movieshare.repository

import com.b1thouse.movieshare.model.Community
import com.b1thouse.movieshare.model.CommunityPerson
import com.b1thouse.movieshare.model.Person
import org.springframework.stereotype.Repository

@Repository
interface CommunityPersonRepository {

    fun insert(person: Person, community: Community)
    fun findByPersonIdAndCommunityId(personId: Long, communityId: Long): CommunityPerson?
    fun findByPerson(personId: Long): List<Community>
}