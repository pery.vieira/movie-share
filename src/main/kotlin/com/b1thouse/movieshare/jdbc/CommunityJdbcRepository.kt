package com.b1thouse.movieshare.jdbc

import com.b1thouse.movieshare.model.Community
import com.b1thouse.movieshare.repository.CommunityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class CommunityJdbcRepository: CommunityRepository {

    @Autowired
    lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    override fun create(community: Community) {
        val query = INSERT
        val parameter = MapSqlParameterSource()

        parameter.addValue("name", community.name)
        parameter.addValue("code", community.code)
        parameter.addValue("created_by", community.createdBy)
        namedParameterJdbcTemplate.update(query, parameter)
    }

    override fun findByCode(code: String): String? {
        val query = FIND_BY_CODE
        val parameter = MapSqlParameterSource()

        parameter.addValue("code", code)
        return try {
            namedParameterJdbcTemplate.queryForObject(query, parameter, String::class.java)
        } catch(ex: EmptyResultDataAccessException){
            return null
        }
    }

    override fun findById(id: Long): Community? {
        val query = FIND_BY_ID
        val parameter = MapSqlParameterSource()

        parameter.addValue("id", id)
        return try {
            namedParameterJdbcTemplate.queryForObject(query, parameter, this::convertToCommunity)
        } catch(ex: EmptyResultDataAccessException){
            return null
        }
    }

    private fun convertToCommunity(rs: ResultSet, rowNum: Int): Community {
        return Community(
                id = rs.getLong("id"),
                name = rs.getString("name"),
                code = rs.getString("code"),
                createdBy = rs.getLong("created_by"),
                createdAt = rs.getTimestamp("created_at").toLocalDateTime(),
                updatedAt = rs.getTimestamp("updated_at").toLocalDateTime(),
                actived = rs.getBoolean("actived")
        )
    }

    companion object {
        const val INSERT = """ INSERT INTO community (name, code, created_by) VALUES (:name, :code, :created_by) ; """

        const val FIND_BY_CODE = """ SELECT code FROM community WHERE code = :code ; """

        const val FIND_BY_ID = """ SELECT * FROM community WHERE id = :id ; """
    }
}