package com.b1thouse.movieshare.jdbc

import com.b1thouse.movieshare.exception.CommunityNotFoundException
import com.b1thouse.movieshare.model.Community
import com.b1thouse.movieshare.model.CommunityPerson
import com.b1thouse.movieshare.model.Person
import com.b1thouse.movieshare.repository.CommunityPersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class CommunityPersonJdbcRepository: CommunityPersonRepository {

    @Autowired
    lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    override fun insert(person: Person, community: Community) {
        val query = INSERT
        val parameter = MapSqlParameterSource()

        parameter.addValue("person_id", person.id)
        parameter.addValue("community_id", community.id)
        namedParameterJdbcTemplate.update(query, parameter)
    }

    override fun findByPersonIdAndCommunityId(personId: Long, communityId: Long): CommunityPerson? {
        val query = FIND_BY_PERSON_COMMUNITY
        val parameter = MapSqlParameterSource()

        parameter.addValue("person_id", personId)
        parameter.addValue("community_id", communityId)
        return try {
            namedParameterJdbcTemplate.queryForObject(query, parameter, this::convertToCommunityPerson)
        } catch(ex: EmptyResultDataAccessException){
            return null
        }
    }

    override fun findByPerson(personId: Long): List<Community> {
    val query = FIND_BY_PERSON_ID
    val parameter = MapSqlParameterSource()

    parameter.addValue("person_id", personId)

    return namedParameterJdbcTemplate.query(query, parameter, this::convertToCommunity)
    }

    private fun convertToCommunityPerson(rs: ResultSet, rowNum: Int): CommunityPerson {
        return CommunityPerson(
                id = rs.getLong("id"),
                personId = rs.getLong("person_id"),
                communityId = rs.getLong("community_id")
        )
    }

    private fun convertToCommunity(rs: ResultSet, rowNum: Int): Community {
        return Community(
                id = rs.getLong("id"),
                name = rs.getString("name"),
                code = rs.getString("code"),
                createdBy = rs.getLong("created_by")
        )
    }

    companion object {
        const val INSERT = """ INSERT INTO community_person (person_id, community_id) VALUES (:person_id, :community_id) ; """
        const val FIND_BY_PERSON_COMMUNITY = """ SELECT * FROM community_person WHERE person_id = :person_id AND community_id = :community_id """
        const val FIND_BY_PERSON_ID = """ select c.id, c."name", c.created_by, c.code from community_person cp 
                                            inner join person p on cp.person_id  = p.id 
                                            inner join community c on cp.community_id = c.id 
                                            where p.id  = :person_id """
    }
}