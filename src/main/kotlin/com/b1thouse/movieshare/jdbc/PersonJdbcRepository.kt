package com.b1thouse.movieshare.jdbc

import com.b1thouse.movieshare.model.Person
import com.b1thouse.movieshare.repository.PersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class PersonJdbcRepository: PersonRepository {

    @Autowired
    lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    override fun create(person: Person) {
        val query = INSERT
        val parameter = MapSqlParameterSource()

        parameter.addValue("name", person.name)
        parameter.addValue("code", person.code)
        namedParameterJdbcTemplate.update(query, parameter)
    }

    override fun findByCode(code: String): String?{
        val query = FIND_BY_CODE
        val parameter = MapSqlParameterSource()

        parameter.addValue("code", code)
        return try {
            namedParameterJdbcTemplate.queryForObject(query, parameter, String::class.java)
        } catch(ex: EmptyResultDataAccessException){
            return null
        }
    }

    override fun findById(id: Long): Person? {
        val query = FIND_BY_ID
        val parameter = MapSqlParameterSource()

        parameter.addValue("id", id)
        return namedParameterJdbcTemplate.queryForObject(query, parameter, this::convertToPerson)
    }

    private fun convertToPerson(rs: ResultSet, rowNum: Int): Person {
        return Person(
                id = rs.getLong("id"),
                name = rs.getString("name"),
                code = rs.getString("code"),
                createdAt = rs.getTimestamp("created_at").toLocalDateTime(),
                updatedAt = rs.getTimestamp("updated_at").toLocalDateTime(),
                actived = rs.getBoolean("actived")
        )
    }

    companion object {
        const val INSERT = """ INSERT INTO person (name, code) VALUES (:name, :code) ; """

        const val FIND_BY_CODE = """ SELECT code FROM person WHERE code = :code ; """

        const val FIND_BY_ID = """ SELECT * FROM person WHERE id = :id ; """
    }
}