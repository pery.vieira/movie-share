package com.b1thouse.movieshare.service

import com.b1thouse.movieshare.exception.PersonNotFoundException
import com.b1thouse.movieshare.model.Person
import com.b1thouse.movieshare.repository.PersonRepository
import com.b1thouse.movieshare.utils.generateCode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PersonService {

    @Autowired
    lateinit var personRepository: PersonRepository

    fun create(person: Person) {
        var code: String
        do{
            code = generateCode()
        } while (personRepository.findByCode(code) != null)

        person.code = code
        personRepository.create(person)
    }

    fun findById(id: Long): Person {
        return personRepository.findById(id) ?: throw PersonNotFoundException(message = "Person not found for id: $id")
    }
}