package com.b1thouse.movieshare.service

import com.b1thouse.movieshare.exception.CommunityNotFoundException
import com.b1thouse.movieshare.model.Community
import com.b1thouse.movieshare.repository.CommunityPersonRepository
import com.b1thouse.movieshare.repository.CommunityRepository
import com.b1thouse.movieshare.utils.generateCode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException.BadRequest

@Service
class CommunityService {

    @Autowired
    lateinit var communityRepository: CommunityRepository

    @Autowired
    lateinit var communityPersonRepository: CommunityPersonRepository

    @Autowired
    lateinit var personService: PersonService

    fun create(community: Community){
        var code: String
        do{
            code = generateCode()
        } while (communityRepository.findByCode(code) != null)

        community.code = code
        communityRepository.create(community)

        val creator = personService.findById(community.createdBy)

        communityPersonRepository.insert(creator!!, community)

    }

    fun findById(communityId: Long): Community{
        return communityRepository.findById(communityId) ?: throw IllegalStateException("Community not found for id: $communityId")

    }

    fun join(personId: Long, communityId: Long){
        val person = personService.findById(personId)
        val community = findById(communityId)

        communityPersonRepository.findByPersonIdAndCommunityId(personId, communityId)?.let {
            throw IllegalArgumentException("Person $personId already on community $communityId")
        }
        communityPersonRepository.insert(person, community)
    }

    fun findByPersonId(personId: Long): List<Community> {
        return communityPersonRepository.findByPerson(personId).takeIf { it.isNotEmpty() }
                ?: throw CommunityNotFoundException(message = "Grupo não encontrado para o personId=$personId")
    }
}