package com.b1thouse.movieshare.controller

import com.b1thouse.movieshare.model.Community
import com.b1thouse.movieshare.service.CommunityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/community")
class CommunityController {
    @Autowired
    lateinit var communityService: CommunityService

    @PostMapping("/join")
    fun join(@RequestParam personId: Long,
             @RequestParam communityId: Long): ResponseEntity<String>{

        try{
            communityService.join(personId, communityId)
        } catch (ex:Exception){
            return ResponseEntity.badRequest().body("Usuário já está no grupo")
        }

        return ResponseEntity.ok("Inserido no Grupo")
    }

    @GetMapping("/person")
    fun getByPersonId(@RequestParam personId: Long): ResponseEntity<List<Community>> {
        return ResponseEntity.ok(communityService.findByPersonId(personId))
    }
}