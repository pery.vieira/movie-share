package com.b1thouse.movieshare.controller

import com.b1thouse.movieshare.model.Person
import com.b1thouse.movieshare.service.PersonService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/person")
class PersonController{

    @Autowired
    lateinit var personService: PersonService

    @GetMapping
    fun getAll(): ResponseEntity<Person>{
        return ResponseEntity.status(HttpStatus.OK).body(personService.findById(0))
    }

    @PostMapping
    fun create(@RequestBody person: Person) : ResponseEntity<String>{
        personService.create(person)
        return ResponseEntity.status(201).body("Person created")
    }
}