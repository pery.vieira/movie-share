package com.b1thouse.movieshare.utils

const val caracteres =  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

fun generateCode(): String{
    return (1..5)
            .map { caracteres.random() }
            .joinToString("")
}