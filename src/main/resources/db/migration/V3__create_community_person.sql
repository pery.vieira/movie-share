CREATE SEQUENCE IF NOT EXISTS community_person_seq_id
    increment 1
    MINVALUE 1
    start 1;

CREATE TABLE IF NOT EXISTS community_person (
    id integer primary key default nextval('community_person_seq_id'),
    person_id INTEGER REFERENCES person(id),
    community_id INTEGER REFERENCES community(id)
);