CREATE SEQUENCE IF NOT EXISTS community_seq_id
    increment 1
    MINVALUE 1
    start 1;

CREATE TABLE IF NOT EXISTS community (
    id integer primary key default nextval('community_seq_id'),
    name varchar(255) NOT NULL,
    code varchar(5) NOT NULL,
    created_by INTEGER REFERENCES person(id),
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now(),
    actived boolean NOT NULL DEFAULT true
);
