CREATE SEQUENCE IF NOT EXISTS person_seq_id
    increment 1
    MINVALUE 1
    start 1;

CREATE TABLE IF NOT EXISTS person (
    id integer primary key default nextval('person_seq_id'),
    name varchar(255) NOT NULL,
    code varchar(5) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now(),
    actived boolean NOT NULL DEFAULT true
);
